import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, CreateDateColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Reservation extends BaseEntity {

    @ApiProperty({
        description: 'id'
    })
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({
        description: 'id арендуемого автомобиля',
        type: 'integer'
    })
    @Column()
    carId: number;

    @ApiProperty({
        description: 'id клиента, который арендует автомобиль',
        type: 'integer'
    })
    @Column()
    clientId: number;

    @ApiProperty({
        description: 'Дата и время начала аренды',
        type: 'string',
        format: 'date-time'
    })
    @Column({
        type: 'datetime'
    })
    startDate: Date;

    @ApiProperty({
        description: 'Дата и время окончания аренды',
        type: 'string',
        format: 'date-time'
    })
    @Column({
        type: 'datetime'
    })
    endDate: Date;

    @ApiProperty({
        description: 'Количество полных дней аренды, которое вычисляется по внутренним правилам системы бронирования. Может со временем меняться, поэтому храним отдельно'
    })
    @Column({
        type: 'integer'
    })
    totalDays: number;

    @ApiProperty({
        description: 'Общая стоимость аренды за указанное количество дней: вычисляется по формуле актуальной на момент создания записи. Тоже может меняться поэтому храним отдельно'
    })
    @Column({
        type: 'integer'
    })
    totalCost: number;

    @ApiProperty({
        description: 'Дата создания записи'
    })
    @CreateDateColumn()
    createdAt: Date;

}