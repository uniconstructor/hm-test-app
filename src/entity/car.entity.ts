import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Car extends BaseEntity {

    @ApiProperty({
        description: 'id'
    })
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({
        description: 'Госномер'
    })
    @Column()
    registrationNumber: string;

    @ApiProperty({
        description: 'Модель автомобиля'
    })
    @Column()
    model: string;

}