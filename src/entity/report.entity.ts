import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, CreateDateColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Report extends BaseEntity {

    @ApiProperty({
        description: 'id'
    })
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({
        description: 'id автомобиля (используется в отчетах по одному автомобилю)',
        type: 'integer'
    })
    @Column()
    carId?: number;

    @ApiProperty({
        description: 'Тип отчета: full (все автомобили), short (один автомобиль)'
    })
    @Column()
    type: string;

    @ApiProperty({
        description: 'Сериализованные данные отчета в json'
    })
    @Column()
    data: string;

    @ApiProperty({
        description: 'Дата начала периода отчета (всегда первый день месяца)',
        type: 'string',
        format: 'date-time'
    })
    @Column({
        type: 'datetime'
    })
    startDate: Date;

    @ApiProperty({
        description: 'Дата окончания периода отчета (всегда последний день месяца)',
        type: 'string',
        format: 'date-time'
    })
    @Column({
        type: 'datetime'
    })
    endDate: Date;

    @ApiProperty({
        description: 'Общий процент использования всего автопарка (тип отчета full) или одного автомобиля (тип отчета short)'
    })
    @Column()
    totalUsage: number;

    @ApiProperty({
        description: 'Дата формирования отчета'
    })
    @CreateDateColumn()
    createdAt: Date;

}