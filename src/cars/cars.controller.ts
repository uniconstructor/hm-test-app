import { Controller, Get, Param, HttpException } from '@nestjs/common';
import { ApiProperty, ApiTags, ApiOkResponse, ApiDefaultResponse, ApiNotFoundResponse } from '@nestjs/swagger';
import { EntityNotFoundError} from 'typeorm';
import { Car } from './../entity/car.entity';
import { CarsService } from './cars.service';

@ApiTags('cars')
@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  @ApiOkResponse({
    description: 'Список всех автомобилей',
    type: [Car],
  })
  @Get()
  findAll(): Promise<Car[]> {
    return this.carsService.findAll();
  }

  @ApiOkResponse({
    description: 'Данные автомобиля',
    type: Car,
  })
  @ApiNotFoundResponse({
    description: 'Автомобиль с указанным id не найден',
  })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Car> {
    try {
        let result = await this.carsService.findOne(id);
        return result
    } catch (error) {
        if (error instanceof EntityNotFoundError) {
            throw new HttpException('Запись с таким id не найдена', 404)
        }
        throw error
    }
}
}