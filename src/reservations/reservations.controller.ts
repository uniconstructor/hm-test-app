import { Body, Controller, Delete, Get, Options, Param, Post, Put, HttpException } from '@nestjs/common';
import { 
    ApiProperty, 
    ApiTags, 
    ApiCreatedResponse, 
    ApiBadRequestResponse, 
    ApiUnprocessableEntityResponse, 
    ApiDefaultResponse, 
    ApiNotFoundResponse, 
    ApiOkResponse } from '@nestjs/swagger';
import { EntityNotFoundError} from 'typeorm';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { Reservation } from '../entity/reservation.entity';
import { ReservationsService } from './reservations.service';

@ApiTags('reservations')
@Controller('reservations')
export class ReservationsController {
    constructor(private readonly reservationsService: ReservationsService) {}

    @Post()
    @ApiCreatedResponse({ 
        description: 'Бронирование успешно зарегистрировано',
        type: Reservation,
    })
    @ApiBadRequestResponse({
        description: 'Ошибка при парсинге JSON',
    })
    @ApiUnprocessableEntityResponse({
        description: 'Ошибка при валидации',
    })
    create(@Body() createReservationDto: CreateReservationDto): Promise<Reservation> {
        return this.reservationsService.create(createReservationDto);
    }

    @ApiOkResponse({
        description: 'Данные бронирования',
        type: [Reservation],
    })
    @Get()
    findAll(): Promise<Reservation[]> {
        return this.reservationsService.findAll();
    }

    @ApiOkResponse({
        description: 'Данные бронирования',
        type: Reservation,
    })
    @ApiNotFoundResponse({
        description: 'Запись с таким id не найдена',
    })
    @Get(':id')
    async findOne(@Param('id') id: string): Promise<Reservation> {
        try {
            let result = await this.reservationsService.findOne(id);
            return result
        } catch (error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException('Запись с таким id не найдена', 404)
            }
            throw error
        }
    }

    @ApiOkResponse({ description: 'Запись успешно удалена' })
    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
        return this.reservationsService.remove(id);
    }

    @ApiOkResponse({
        description: 'Стоимость бронирования',
        type: Number,
    })
    @Get('/calculateCost/:totalDays')
    calculateCost(@Param('totalDays') totalDays: number): number {
        console.log(totalDays)
        return this.reservationsService.getDaysTotalCost(totalDays)
    }
}