
import { ApiProperty } from '@nestjs/swagger';

export class CreateReservationDto {
    @ApiProperty({
        description: 'id арендуемого автомобиля',
        type: 'integer',
    })
    carId: number;

    @ApiProperty({
        description: 'id клиента, который арендует автомобиль',
        type: 'integer'
    })
    clientId: number;

    @ApiProperty({
        description: 'Дата и время начала аренды',
        type: 'string',
        format: 'date-time'
    })
    startDate: Date;

    @ApiProperty({
        description: 'Дата и время окончания аренды',
        type: 'string',
        format: 'date-time'
    })
    endDate: Date
}