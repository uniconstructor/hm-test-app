import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { Reservation } from '../entity/reservation.entity';

@Injectable()
export class ReservationsService {
    constructor(
        @InjectRepository(Reservation)
        private readonly reservationsRepository: Repository<Reservation>,
    ) {}

    create(createReservationDto: CreateReservationDto): Promise<Reservation> {
        const reservation     = new Reservation();
        reservation.carId     = createReservationDto.carId;
        reservation.clientId  = createReservationDto.clientId;
        reservation.startDate = createReservationDto.startDate;
        reservation.endDate   = createReservationDto.endDate;
        // по переданным датам вычисляем общее количество дней аренды
        reservation.totalDays = this.getTotalDays(reservation);
        // по количеству дней и тарифной сетке вычисляем общую стоимость аренды за весь период
        reservation.totalCost = this.getTotalCost(reservation);
        // все обязательные параметры проставлены, можно сохранять
        return this.reservationsRepository.save(reservation);
    }

    async findAll(): Promise<Reservation[]> {
        return this.reservationsRepository.find();
    }

    findOne(id: string | number): Promise<Reservation> {
        return this.reservationsRepository.findOneOrFail(id);
    }

    async remove(id: string | number): Promise<void> {
        await this.reservationsRepository.delete(id);
    }

    /**
     * Вычислить количество дней в периоде бронирования по указанным датам
     * 
     * @param reservation объект с создаваемой записью бронирования
     * @returns 
     */
    getTotalDays(reservation: Reservation): number {
        const diff     = Math.abs(new Date(reservation.endDate).getTime() - new Date(reservation.startDate).getTime());
        const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        return diffDays
    }

    /**
     * Получить полную стоимость аренды
     * 
     * @param reservation объект с создаваемой записью бронирования
     */
    getTotalCost(reservation: Reservation): number {
        const totalDays = this.getTotalDays(reservation)
        const totalCost = this.getDaysTotalCost(totalDays)
        return totalCost
    }

    /**
     * Получить полную стоимость аренды по количеству дней
     * 
     * @param totalDays количество дней бронирования
     */
     getDaysTotalCost(totalDays: number): number {
        let totalCost   = 0
        for (let dayNumber = 1; dayNumber <= totalDays; dayNumber++) {
            const dailyCost = this.getCostOfTheDay(dayNumber)
            totalCost += dailyCost
        }
        return totalCost
    }

    /**
     * Получить стоимость одного дня аренды
     * 
     * @param dayNumber номер дня в периоде аренды: нумерация с единицы
     * @returns 
     */
    getCostOfTheDay(dayNumber: number): number {
        const BASE_COST = 1000
        if ((dayNumber >= 1) &&(dayNumber <= 4)) {
            return BASE_COST
        } else if ((dayNumber >= 5) && (dayNumber <= 9)) {
            return parseInt((BASE_COST * 0.95).toFixed(0))
        } else if ((dayNumber >= 10) && (dayNumber <= 17)) {
            return parseInt((BASE_COST * 0.90).toFixed(0))
        } else if ((dayNumber >= 18) && (dayNumber <= 30)) {
            return parseInt((BASE_COST * 0.85).toFixed(0))
        } else {
            throw new Error('Некорректное значение срока аренды')
        }
    }
}