import "reflect-metadata";
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { getConnection } from "typeorm";
import { Car } from './entity/car.entity';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    // Swagger API
    const config = new DocumentBuilder()
    .setTitle('Расчет стоимости аренды автомобиля')
    .setDescription('API')
    .setVersion('1.0')
    .addTag('cars')
    .addTag('reservations')
    .addTag('reports')
    .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    // заполнение базы изначальными данными
    const connection = getConnection();
    const carRepository = connection.getRepository(Car);
    // используемые в прокате модели машин
    const carModels = [
        "SANTA FE",
        "TUCSON",
        "CRETA",
        "SONATA",
        "H-1",
    ];
    let carId = 1;
    // заполнение тестового списка автомобилей
    for (const carModel of carModels) {
        const newCar              = new Car();
        newCar.model              = carModel;
        newCar.registrationNumber = `a${carId}${carId}${carId}aa199`;
        await carRepository.save(newCar);
        carId += 1;
    }
    let savedCars = await carRepository.find();
    console.log("Тестовый список автомобилей: ", savedCars);

    // запуск веб-сервера
    await app.listen(3000);
}
bootstrap();
