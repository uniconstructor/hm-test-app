import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateReportDto } from './dto/create-report.dto';
import { Report } from '../entity/report.entity';

@Injectable()
export class ReportsService {
    constructor(
        @InjectRepository(Report)
        private readonly reportsRepository: Repository<Report>,
    ) {}

    async create(createReportDto: CreateReportDto): Promise<Report> {
        const report     = new Report();
        report.carId     = createReportDto.carId;
        report.startDate = createReportDto.startDate;
        report.endDate   = createReportDto.endDate;
        report.type      = 'full';
        if (report.carId > 0) {
            report.type = 'short';
        }
        // собираем статистику бронирования за период отчета
        const reportData = {}; // await this.collectReportData(report);
        report.data       = JSON.stringify(reportData);
        report.totalUsage = 0; // this.calculateTotalUsage(report);
        
        return this.reportsRepository.save(report);
    }

    async findAll(): Promise<Report[]> {
        return this.reportsRepository.find();
    }

    findOne(id: string | number): Promise<Report> {
        return this.reportsRepository.findOneOrFail(id);
    }

    async remove(id: string | number): Promise<void> {
        await this.reportsRepository.delete(id);
    }

}