
import { ApiProperty } from '@nestjs/swagger';

export class CreateReportDto {
    @ApiProperty({
        description: 'id автомобиля (используется в отчетах по одному автомобилю)',
        type: 'integer'
    })
    carId?: number;

    @ApiProperty({
        description: 'Дата начала периода отчета (всегда первый день месяца)',
        type: 'string',
        format: 'date-time'
    })
    startDate: Date;

    @ApiProperty({
        description: 'Дата окончания периода отчета (всегда последний день месяца)',
        type: 'string',
        format: 'date-time'
    })
    endDate: Date;
}