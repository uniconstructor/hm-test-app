import { Body, Controller, Delete, Get, Param, Post, HttpException } from '@nestjs/common';
import { 
    ApiProperty, 
    ApiTags, 
    ApiCreatedResponse, 
    ApiBadRequestResponse, 
    ApiUnprocessableEntityResponse, 
    ApiDefaultResponse, 
    ApiNotFoundResponse, 
    ApiOkResponse } from '@nestjs/swagger';
import { EntityNotFoundError} from 'typeorm';
import { CreateReportDto } from './dto/create-report.dto';
import { Report } from '../entity/report.entity';
import { ReportsService } from './reports.service';

@ApiTags('reports')
@Controller('reports')
export class ReportsController {
    constructor(private readonly reportsService: ReportsService) {}

    @Post()
    @ApiCreatedResponse({ 
        description: 'Отчет успешно создан',
        type: Report,
    })
    @ApiBadRequestResponse({
        description: 'Ошибка при парсинге JSON',
    })
    @ApiUnprocessableEntityResponse({
        description: 'Ошибка при валидации',
    })
    create(@Body() createReportDto: CreateReportDto): Promise<Report> {
        return this.reportsService.create(createReportDto);
    }

    @ApiOkResponse({
        description: 'Данные отчетов',
        type: [Report],
    })
    @Get()
    findAll(): Promise<Report[]> {
        return this.reportsService.findAll();
    }

    @ApiOkResponse({
        description: 'Данные отчета',
        type: Report,
    })
    @ApiNotFoundResponse({
        description: 'Запись с таким id не найдена',
    })
    @Get(':id')
    async findOne(@Param('id') id: string): Promise<Report> {
        try {
            let result = await this.reportsService.findOne(id);
            return result
        } catch (error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException('Запись с таким id не найдена', 404)
            }
            throw error
        }
    }

    @ApiOkResponse({ description: 'Запись успешно удалена' })
    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
        return this.reportsService.remove(id);
    }
}